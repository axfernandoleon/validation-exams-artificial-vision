#!/usr/bin/env python
# -*- coding: utf-8 -*-
import  sys 
from PyQt5.QtWidgets import QApplication, QMainWindow, QDesktopWidget,QMessageBox
from PyQt5 import uic

import webcam2
import ctypes #GetSystem
class Ventana(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        
        uic.loadUi("helloWorld.ui",self)
        self.setWindowTitle("CVision-beta")

        #Maximizado
        #self.showMaximized()

        #Redisenio x,y
        #self.setMinimumSize(500,500)
        #self.setMaximumSize(500,500)

        #centrar
        self.center()
        # Boton Salir
        self.btnSalir.clicked.connect(exit)
        self.valuesBox()
        # Boton para numero de opciones

        self.horizontalSlider.valueChanged.connect(self.valuesBox)
        #Botones 1 a 1
        self.sb1.valueChanged.connect(self.valueBox1)
        self.sb2.valueChanged.connect(self.valueBox2)
        self.sb3.valueChanged.connect(self.valueBox3)
        self.sb4.valueChanged.connect(self.valueBox4)
        self.sb5.valueChanged.connect(self.valueBox5)
        self.sb6.valueChanged.connect(self.valueBox6)
        self.sb7.valueChanged.connect(self.valueBox7)
        self.sb8.valueChanged.connect(self.valueBox8)
        self.sb9.valueChanged.connect(self.valueBox9)
        self.sb10.valueChanged.connect(self.valueBox10)

        #datos tomados
        self.btnCargar.clicked.connect(self.opciones)
        
        self.btntriangle.setEnabled(False)
    	self.activate()
    	self.numberQ()


    def activate(self):
    	
        print self.gpbox10.isChecked()
        
        self.gpbox10.setEnabled(False)
        #self.gpbox10.hide()
        print self.gpbox1.isChecked()
    def numberQ(self):
    	print self.SliderP.value()

    


    def opciones(self):
        print "1"
        self.opcEvt(self.gpbox1_A,self.gpbox1_B,self.gpbox1_C,self.gpbox1_D)
        print "2"
        self.opcEvt(self.gpbox2_A,self.gpbox2_B,self.gpbox2_C,self.gpbox2_D)
        print "3"
        self.opcEvt(self.gpbox3_A,self.gpbox3_B,self.gpbox3_C,self.gpbox3_D)
        print "4"
        self.opcEvt(self.gpbox4_A,self.gpbox4_B,self.gpbox4_C,self.gpbox4_D)
        print "5"
        self.opcEvt(self.gpbox5_A,self.gpbox5_B,self.gpbox5_C,self.gpbox5_D)
        print "6"
        self.opcEvt(self.gpbox6_A,self.gpbox6_B,self.gpbox6_C,self.gpbox6_D)
        print "7"
        self.opcEvt(self.gpbox7_A,self.gpbox7_B,self.gpbox7_C,self.gpbox7_D)
        print "8"
        self.opcEvt(self.gpbox8_A,self.gpbox8_B,self.gpbox8_C,self.gpbox8_D)
        print "9"
        self.opcEvt(self.gpbox9_A,self.gpbox9_B,self.gpbox9_C,self.gpbox9_D)
        print "10"
        self.opcEvt(self.gpbox10_A,self.gpbox10_B,self.gpbox10_C,self.gpbox10_D)
        self.hide()
        if webcam2.camara():
        	self.hide()
        	exit()
        else:
        	self.show()
    	





    def opcEvt(self, gpb1,gpb2,gpb3,gpb4):

        if gpb1.isChecked()==True and gpb1.isEnabled()==True:
            print "A"
        if gpb2.isChecked()==True and gpb2.isEnabled()==True:
            print "B"
        if gpb3.isChecked()==True and gpb3.isEnabled()==True:
            print "C"
        if gpb4.isChecked()==True and gpb4.isEnabled()==True:
            print "D"




    def valuesBox(self):
        self.valueQSpinBox(self.sb1 ,self.gpbox1_C, self.gpbox1_D)
        self.valueQSpinBox(self.sb2 ,self.gpbox2_C,self.gpbox2_D)
        self.valueQSpinBox(self.sb3 ,self.gpbox3_C,self.gpbox3_D)
        self.valueQSpinBox(self.sb4 ,self.gpbox4_C,self.gpbox4_D)
        self.valueQSpinBox(self.sb5 ,self.gpbox5_C,self.gpbox5_D)
        self.valueQSpinBox(self.sb6 ,self.gpbox6_C,self.gpbox6_D)
        self.valueQSpinBox(self.sb7 ,self.gpbox7_C,self.gpbox7_D)
        self.valueQSpinBox(self.sb8 ,self.gpbox8_C,self.gpbox8_D)
        self.valueQSpinBox(self.sb9 ,self.gpbox9_C,self.gpbox9_D)
        self.valueQSpinBox(self.sb10 ,self.gpbox10_C,self.gpbox10_D)

    def valueBox1(self):
        self.valueQSpinBox(self.sb1 ,self.gpbox1_C, self.gpbox1_D)
    def valueBox2(self):
        self.valueQSpinBox(self.sb2 ,self.gpbox2_C, self.gpbox2_D)
    def valueBox3(self):
        self.valueQSpinBox(self.sb3 ,self.gpbox3_C, self.gpbox3_D)
    def valueBox4(self):
        self.valueQSpinBox(self.sb4 ,self.gpbox4_C, self.gpbox4_D)
    def valueBox5(self):
        self.valueQSpinBox(self.sb5 ,self.gpbox5_C, self.gpbox5_D)
    def valueBox6(self):
        self.valueQSpinBox(self.sb6 ,self.gpbox6_C, self.gpbox6_D)
    def valueBox7(self):
        self.valueQSpinBox(self.sb7 ,self.gpbox7_C, self.gpbox7_D)
    def valueBox8(self):
        self.valueQSpinBox(self.sb8 ,self.gpbox8_C, self.gpbox8_D)
    def valueBox9(self):
        self.valueQSpinBox(self.sb9 ,self.gpbox9_C, self.gpbox9_D)
    def valueBox10(self):
        self.valueQSpinBox(self.sb10 ,self.gpbox10_C, self.gpbox10_D)

    def valueQSpinBox(self,sb, gpb , gpb2):
        if sb.value()==2:
            
            gpb.setCheckable(False)
            gpb2.setCheckable(False)

            gpb.setEnabled(False)
            gpb2.setEnabled(False)
            
        elif sb.value()==3:

            gpb.setCheckable(True)
            gpb2.setCheckable(False)

           
            gpb.setEnabled(True)
            gpb2.setEnabled(False)
        else:
            gpb.setCheckable(True)
            gpb2.setCheckable(True)
            gpb.setEnabled(True)
            gpb2.setEnabled(True)
    
       

    #centrar ventana
    def center(self):
        resolution = QDesktopWidget().screenGeometry()
        #print resolution.width()
        #print resolution.height()
        self.move(resolution.width()/2-(self.frameSize().width() / 2),resolution.height() / 2-(self.frameSize().height() / 2))
    

app= QApplication(sys.argv)
_ventana= Ventana()

_ventana.show()
app.exec_()
