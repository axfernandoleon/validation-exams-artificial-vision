# -*- coding: utf-8 -*-
"""
Created on Wed Feb 04 23:50:34 2015

@author: lrbarba
"""
import numpy as np
import cv2 # importa la libreria de Opencv
import sys

if len(sys.argv) < 2:
 # with no arguments, use this default file
    image=cv2.imread('/home/fernando/Documentos/gp 2.2/code/images/coins.png')  #Cargar imagen
elif len(sys.argv) == 2:
    filename = sys.argv[1]
else:
    sys.exit("Expecting a single image file argument")


textColor = (240, 64, 128)
#Escribir texto en pantalla
cv2.putText(image, "Rex", (250, 200),cv2.FONT_HERSHEY_PLAIN, 3.0, textColor,thickness=6)
cv2.rectangle(image, (250, 160), (340,200), (128,128,0), 2) #Dibuja un rectangulo en la imagen 

cv2.imshow("Original", image)

cv2.waitKey(0) #Permanece la imagen en pantalla hasta presionar una tecla

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(gray, (11, 11), 0)
cv2.imshow("Image", image)

# The first thing we are going to do is apply edge detection to
# the image to reveal the outlines of the coins
edged = cv2.Canny(blurred, 30, 150)
cv2.imshow("Edges", edged)

# Find contours in the edged image.
# NOTE: The cv2.findContours method is DESTRUCTIVE to the image
# you pass in. If you intend on reusing your edged image, be
# sure to copy it before calling cv2.findContours
(_,cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# How many contours did we find?
print "I count %d coins in this image" % (len(cnts))

coins = image.copy()
cv2.drawContours(coins, cnts, -1, (0, 255, 0), 2)
cv2.imshow("Coins", coins)
cv2.waitKey(0)
# Now, let's loop over each contour
for (i, c) in enumerate(cnts):
	# We can compute the 'bounding box' for each contour, which is
	# the rectangle that encloses the contour
	(x, y, w, h) = cv2.boundingRect(c)

	# Now that we have the contour, let's extract it using array
	# slices
	print "Coin #%d" % (i + 1)
	coin = image[y:y + h, x:x + w]
	cv2.imshow("Coin", coin)

	# Just for fun, let's construct a mask for the coin by finding
	# The minumum enclosing circle of the contour
	mask = np.zeros(image.shape[:2], dtype = "uint8")
	((centerX, centerY), radius) = cv2.minEnclosingCircle(c)
	cv2.circle(mask, (int(centerX), int(centerY)), int(radius), 255, -1)
	mask = mask[y:y + h, x:x + w]
	cv2.imshow("Masked Coin", cv2.bitwise_and(coin, coin, mask = mask))
	cv2.waitKey(0)
