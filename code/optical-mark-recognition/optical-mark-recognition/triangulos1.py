#Algoritmo de deteccion de triangulos
#Por Glar3
#
#
#Detecta triangulos azules
  
#Librerias
import cv2
import numpy as np
  
#Iniciar camara
video = cv2.VideoCapture(0)


while True:

    #Se captura el video de la webcam

    ret,im = video.read()
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    #Se muestra el video  donde se pasa im que es la lectura del video de la webcam.

    cv2.imshow('Prueba de video',gray)

    #Se captura la tecla de escape del teclado

    

    tecla = cv2.waitKey(10)

    if tecla == 27:

        #Si es la tecla escape se termina el ciclo

        break

    #Si la tecla es el espacio en blanco se captura una imagen del video.

    if tecla == ord(' '):

        cv2.imwrite('captura_img.jpg',gray)
        break

image = cv2.imread('captura_img.jpg')
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image, (11, 11), 0)
cv2.imshow("Image", image)

edged=cv2.Canny(blurred, 30,150)
cv2.imshow("Edges", edged)
#imagen y se hace copia, func recupera solo bordes externos, comprime segmentos horizon y vert
(_,cnts,_)= cv2.findContours(edged.copy(),cv2.RETR_EXTERNAL,
   cv2.CHAIN_APPROX_SIMPLE)

print "I count %d coins in this image " % (len(cnts))

coins= image.copy()
# imagen,contornos, numero de contornos -1 todos, color , espesor
cv2.drawContours(coins,cnts,-1,(255,0,0),2)

cv2.imshow("Coins",coins)

cv2.waitKey(0)
  
cv2.destroyAllWindows()