import  cv2
import numpy as np




numero1 = int(input("ELiga la camara  que va Utilizar 0 camara interna  y 1 camara externa: "))


cam = cv2.VideoCapture(numero1)


kernel=np.ones((5,5),np.uint8)
ramp_frames = 30
while (True): 
	ret,frame=cam.read()
	rangomax=np.array([254,254,254])
	rangomin=np.array([209,193,181])
	mascara=cv2.inRange(frame,rangomin,rangomax)
	opening=cv2.morphologyEx(mascara, cv2.MORPH_OPEN, kernel)
	x,y,w,h=cv2.boundingRect(opening)
	cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),3)
	cv2.circle(frame,(x+w/2,y+h/2),5,(0,0,255),-1)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	cv2.imshow('camara',gray)
	k=cv2.waitKey(1) & 0xFF
	print ("x",x,"y",y,"w",w,"h",h)

	if x>162 and x<290and y>10 and y<152 and w>150and w<325 and h>200 and h<350:


		def get_image():
		 # read is the easiest way to get a full image out of a VideoCapture object.
		 retval, im = cam.read()
		 return im

		# Ramp the camera - these frames will be discarded and are only used to allow v4l2
		# to adjust light levels, if necessary
		for i in xrange(ramp_frames):
		 temp = get_image()
		print("Taking image...")
		# Take the actual image we want to keep
		camera_capture = get_image()
		file = "test_image.jpg"
		# A nice feature of the imwrite method is that it will automatically choose the
		# correct format based on the file extension you provide. Convenient!
		cv2.imwrite(file, camera_capture)

		# You'll want to release the camera, otherwise you won't be able to create a new
		# capture object until your script exits
		
		break
	if k==27:
		break
