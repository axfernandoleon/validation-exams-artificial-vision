import numpy as np
import cv2

image = cv2.imread('captura_img.jpg')

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image, (5, 5), 0)
cv2.imshow("Image", image)

edged=cv2.Canny(blurred, 30,150)
cv2.imshow("Edges", edged)
#imagen y se hace copia, func recupera solo bordes externos, comprime segmentos horizon y vert
(_,cnts,_)= cv2.findContours(edged.copy(),cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
res=0
for cnt in cnts:
    approx = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
    print len(approx)

    if len(approx)==3:
        print "triangle"
        cv2.drawContours(image,[cnt],-1,(0,255,0),-1)
        res=1
    elif len(approx)==4:
        print "square"
        cv2.drawContours(image,[cnt],-1,(0,0,255),-1)
        res=2

if res==1:
	print "Un Triangulo"
elif res==2:
	print "UN CuADRADO"

cv2.imshow('img',image)
cv2.waitKey(0)
cv2.destroyAllWindows()