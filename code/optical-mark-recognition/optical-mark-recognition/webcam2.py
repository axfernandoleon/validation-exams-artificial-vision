#!/usr/bin/env python

#Se importa cv2.

import cv2



def camara():
    # se crea la instancia de la captura de Video.

    video = cv2.VideoCapture(1)

    #Se define un ciclo.

    while True:

        #Se captura el video de la webcam

        ret,im = video.read()
        gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        #Se muestra el video  donde se pasa im que es la lectura del video de la webcam.

        cv2.imshow('Prueba de video',im)
        cv2.moveWindow('Prueba de video', 500, 150)
        #Se captura la tecla de escape del teclado

        

        tecla = cv2.waitKey(10)

        if tecla == 27:

            #Si es la tecla escape se termina el ciclo

            break

        #Si la tecla es el espacio en blanco se captura una imagen del video.

        if tecla == ord(' '):

            cv2.imwrite('captura_img.jpg',im)
    
    cv2.destroyAllWindows()
 
camara()
