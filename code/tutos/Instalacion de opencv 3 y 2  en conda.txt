Instalacion de opencv 3 y 2  en conda

http://stackoverflow.com/questions/23119413/how-to-install-python-opencv-through-conda

OPENCV3
	conda install -c https://conda.anaconda.org/menpo opencv3
OPENCV2.4.11
	conda install -c menpo opencv=2.4.11
Imutils
	pip install imutils
Mahotas
	pip install mahotas

Interfaz
	sudo apt-get install qttools5-dev-tools
O
	sudo apt-get install qtcreator

Revisa como funciona:
	https://gist.github.com/arthurbeggs/06df46af94af7f261513934e56103b30/
	pip install --no-deps imutils

Interfaz
	http://askubuntu.com/questions/651461/where-is-qt5-designer
